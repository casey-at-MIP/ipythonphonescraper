# PhoneScraper

Two little scripts to scrape the lineageOS wiki and GSM arena for phone data.
Running 

### Usage
Running GsmArenaScraper.py will create a JSON file for each phone found, and save it in ./Data

It was last run on the 22 March.
Summary in ./phoneData.csv

### Built with
Python3.x
BeautifulSoup4
Requests
```
pip install requests bs4
```

