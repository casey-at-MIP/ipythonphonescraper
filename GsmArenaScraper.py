from bs4 import BeautifulSoup
import requests

import os
import re
import json

def getManufacturerList():
	"""
	Gets a list of URLs. One for each for manufacturer
	"""
	# pull devices page
	manufacturers = requests.get("https://www.gsmarena.com/makers.php3")
	data = manufacturers.text
	soup = BeautifulSoup(data, 'lxml')
	# build device list
	manufacturerList = []
	for link in soup.find_all('a'):
		if '-phones-' in link.get('href'):
			manufacturerList.append(link.get('href'))
			print(link.get('href'))
	
	return manufacturerList

def getManufacturerPages(URL, manufacturer):
	"""
	Gets page URLs from a manufactureres devices listing
	"""
	# create a list for pages
	manufacturerPages = []
	# add the first (and perhaps only) page
	manufacturerPages.append(manufacturer)
	
	# get first page
	phoneList = requests.get(URL + manufacturer)
	data = phoneList.text
	soup = BeautifulSoup(data, 'lxml')
	
	# add the '-f-' to the URL (seems to be consistent)
	manufacturer = manufacturer.replace('-phones-', '-phones-f-')
	manufacturer = manufacturer.replace('.php', '')
	# print("*************", randomManufacturer)
	regex = '(' + manufacturer + ')(.*?)(p)(\d|\d\d)(.)\w+'
	
	# extract pages (if any)
	for link in soup.find_all('a'):
		# print(link.get('href'))
		if re.match(regex, link.get('href')):
			# print("****************************")
			manufacturerPages.append(link.get('href'))
			
	# remove dupes
	manufacturerPages = list(set(manufacturerPages))

	return manufacturerPages


def getDevicePage(PageURL):
	"""
	Gets device links given a page of devices
	"""
	# get first page
	phoneList = requests.get(URL + PageURL)
	data = phoneList.text
	soup = BeautifulSoup(data, 'lxml')
	# ~ print(URL + PageURL, '\n****************************')

	deviceTable = str(soup.find('div', {'id': 'review-body'}))

	soup = BeautifulSoup(deviceTable, 'lxml')

	phoneURLs = []

	for link in soup.find_all('a'):
	#	 print(link.get('href'))
		phoneURLs.append(link.get('href'))

	return phoneURLs


def getPhoneSpecs(URL, phoneURL):
    # get first page
    phonePage = requests.get(URL + phoneURL)
    data = phonePage.text
    soup = BeautifulSoup(data, 'lxml')
    specData = {}
    specData['name'] = soup.find('h1', {'class': 'specs-phone-name-title'}).get_text()
    # <h1 class="specs-phone-name-title" data-spec="modelname">Samsung Galaxy S6 edge+ (USA)</h1>
    # deviceTable = str(soup.find('div', {'id': 'review-body'}))
    
#     print(phoneURL, '\n****************************')
    specsTable = soup.find_all("td", attrs={"data-spec":True})
#     print(str(specsTable), "**************************", )

    for element in specsTable:
#         print(element['data-spec'], ": ", element.get_text())
        specData[element['data-spec']] = element.get_text()
    return specData


 
##
##
## Bringing it all together

URL = "https://www.gsmarena.com/"

# get list of manufacturers
manufacturerList = getManufacturerList()

# speed things up hack - remove everything before 'm'

# ~ # generate alphabet list
# ~ alphabet = list(map(chr, range(97, 109)))
# ~ newManufacturerList = []
# ~ for manufacturer in manufacturerList:
	# ~ for letter in alphabet:
		# ~ if not(manufacturer.startswith(letter)):
			# ~ print(manufacturer, letter)
			# ~ newManufacturerList.append(manufacturer)

# ~ print(newManufacturerList)

# reverse so we scrape from the other end 
manufacturerList.reverse()


for manufacturer in manufacturerList:
	# get page links for all devices from a given manufacturer
	pageList = getManufacturerPages(URL, manufacturer)
	
	for page in pageList:
		# get each device a page
		phoneURLs = getDevicePage(URL + page)
		
		for device in phoneURLs:
			# extract specs & save
			print("Visiting device page: ", device, " (", manufacturer, ")")
			if not os.path.exists('./data/' + device + '.json'):
				specs = getPhoneSpecs(URL, device)
				with open('./data/' + device + '.json', 'w') as fp:
					json.dump(specs, fp)
			else:
				print("Already scraped! :)")
