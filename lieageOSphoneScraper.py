from bs4 import BeautifulSoup
import requests
import json
import time
import sys

# a scraper for getting device info from LineageOS supported devices page,
# and a list of phone specs from GSM Arena

#~ """
#~ This is a reST style.

#~ :param param1: this is a first param
#~ :param param2: this is a second param
#~ :returns: this is a description of what is returned
#~ :raises keyError: raises an exception
#~ """


def pullDevicePage():
	"""
	Get device URL list from LieageOS supported device list.
	(Returned as list)
	"""
	# pull devices page
	supportedDeviceWiki = requests.get("https://wiki.lineageos.org/devices/")
	data = supportedDeviceWiki.text
	soup = BeautifulSoup(data, 'lxml')

	# build device list
	deviceList = []
	for link in soup.find_all('a'):
		if 'device' in link.get('href'):
			deviceList.append(link.get('href'))
			#print(link.get('href'))

	# remove dupes
	deviceList = list(set(deviceList))
	# remove '/devices/' prefix
	deviceList = [device.replace('/devices/', '') for device in deviceList]
	
	return deviceList
	
def visitDevicePage(url):
	# visit device page
	devicePage = requests.get(url)
	data = devicePage.text
	return data


def extractDeviceSpec(devicePage):
	"""
	:param devicePage: URL to device on LieageOS wiki
	"""
	soup = BeautifulSoup(devicePage, 'lxml')
	specData = []
	table = soup.find('table')
	rows = table.find_all('tr')
	
	for row in rows:
		cols = row.find_all('td')
		cols = [ele.text.strip() for ele in cols]
		# split and clean if multiples occur
		if len(cols) >= 2:
			cols[1] = cols[1].split("\n")
			cols[1] = list(filter(None, map(str.strip, cols[1])))
		specData.append([ele for ele in cols if ele]) # Get rid of empty values
		
	# remove empty
	specData = list(filter(None, specData))

	return specData




try: 
	devicePage = pullDevicePage()
	print("Found ", len(devicePage), " pages.")
	
except:
	print("Could not get to LineageOS wiki. :(")

for remaining in range(3, 0, -1):
    sys.stdout.write("\r")
    sys.stdout.write("{:2d} seconds remaining until scrape starts...".format(remaining)) 
    sys.stdout.flush()
    time.sleep(1)


for url in devicePage:
	#~ try:
	print("visiting: ", url)
	data = visitDevicePage("https://wiki.lineageos.org/devices/" + url)
	specs = extractDeviceSpec(data)
	with open('./data/' + url + '.json', 'w') as fp:
		json.dump(specs, fp)

